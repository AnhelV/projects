
function sum(arr) {
    return arr.reduce((acc, item) => {
        if ((item % 2) === 0) {
            acc += item
        }
        return acc;
    },0)
}

export default sum;
